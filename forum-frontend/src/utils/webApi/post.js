import axios from 'axios';
const baseUrl = '/api/posts';

export  const getPostByForum = async(forumId) =>{
    let response = await axios.get(`${baseUrl}/forum/${forumId}`);
    return response.data;
};

export  const searchPosts = async(request) =>{
    let response = await axios.get(`${baseUrl}/search${request}`);
    return response.data;
};
export const getLatestPost=async() =>{
    let response = await axios.get(`${baseUrl}/latest`);
    return response.data;
};
export const getPostById = async(id) =>{
    let response = await axios.get(`${baseUrl}/${id}`);
    return response.data;
};
export const addPost= async(post)=>{
    let response = await axios.post(baseUrl,post);
    return response.data;
}

export const getAllPosts= async(post)=>{
    let response = await axios.get(baseUrl);
    return response.data;
}

export const deletePost = async(id) =>{
    let response = await axios.delete(`${baseUrl}/${id}`);
    return response.data;
};

export const updatePost = async(postData) =>{
    let response = await axios.put(`${baseUrl}/${postData.id}`,postData);
    return response.data;
};