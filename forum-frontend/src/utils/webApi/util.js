import axios from 'axios';

const baseUrl = '/api/utils';

export const uploadFile = async(file)=>{
    let formData = new FormData();
    formData.append("file",file);
    var url = `${baseUrl}/uploadFile`;
    let response = await axios.post(url,formData,{
        headers:{
            "Content-Type": "multipart/form-data"
        }
    });
    return response.data;
}
