import axios from 'axios';
const baseUrl = '/api/likes';

export const addLike= async(like) =>{
    let response = await axios.post(`${baseUrl}`,like);
    return response.data;
};