import axios from 'axios';

export const createRequest = (data,id = 0,page)=>{
 var newServiceRequest = `?data=${data}&page=${page}`;
return newServiceRequest;
}
export const getPaginateData = async(dataType,request) =>{
    let url = `/api/${dataType}${request}`;
    let response = await axios.get(url);
    return response.data;
};