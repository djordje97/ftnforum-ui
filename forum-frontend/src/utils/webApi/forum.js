import axios from 'axios';

const baseUrl = '/api/forums';

export const fetchForums = async() => {
    let response = await axios.get(baseUrl);
    return response.data;
};
export const fetchForum = async(forumId)=>{
    let url = `${baseUrl}/${forumId}`;
    let response = await axios.get(url);
    return response.data;
}

export const createForum = async(forumData)=>{
    let response = await axios.post(baseUrl,forumData);
    return response.data;
}

export const deleteForum = async(forumId)=>{
    let response = await axios.delete(`${baseUrl}/${forumId}`);
    return response.data;
}
export const updateForum = async(forumData)=>{
    let response = await axios.put(`${baseUrl}/${forumData.id}`,forumData);
    return response.data;
}

