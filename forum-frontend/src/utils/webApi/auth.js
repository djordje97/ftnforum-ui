import axios from 'axios';
import {Role} from '../Roles';
const baseUrl="/api/auth";

export const login = async (loginData) =>{
    let response = await axios.post(baseUrl+'/login',loginData);
    var res = response.data;
    if(res.isSuccess == true){
        var loggedUser  = JSON.stringify(res.data.loggedUser);
        localStorage.setItem("token",JSON.stringify(res.data.token));
        localStorage.setItem("loggedUser",btoa(loggedUser));
    }
  
    return response.data;
};
export const changePassword = async (changePasswordData) =>{
    let response = await axios.post(baseUrl+'/changePassword',changePasswordData);
    return response.data;
};
export const getCurrentUser =() =>{
    let user = null;
    let localStorageData = localStorage.getItem("loggedUser");
    if(localStorageData!= null){
        var decodedData = atob(localStorageData);
        user = JSON.parse(decodedData);
    }
   
    return user;
}

export const isAuthenticated  = () =>{
    let token = localStorage.getItem("token");
    let isAuthenticated = token ? true:false;
    return isAuthenticated;
}
export const isAdmin = () =>{
    let isAdmin = false;
    let user = null;
    let localStorageData = localStorage.getItem("loggedUser");
    if(localStorageData!= null){
        var decodedData = atob(localStorageData);
        user = JSON.parse(decodedData);
        if(user.role.name == Role.Admin)
        isAdmin = true;
    }
    return isAdmin;
}