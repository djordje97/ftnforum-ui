import axios from 'axios';
const baseUrl = '/api/replies';

export const addReply= async(reply) =>{
    let response = await axios.post(`${baseUrl}`,reply);
    return response.data;
};
export const  getRepliesByPost = async(postId) =>{
    let response = await axios.get(`${baseUrl}/post/${postId}`);
   return response.data;
}

export const  deleteReply = async(replyId) =>{
    let response = await axios.delete(`${baseUrl}/${replyId}`);
   return response.data;
}
export const  editReply = async(replyData) =>{
    let response = await axios.put(`${baseUrl}/${replyData.id}`,replyData);
   return response.data;
}