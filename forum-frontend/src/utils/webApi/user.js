import axios from 'axios';
const baseUrl = '/api/users';

export const  register = async(user) =>{
    let response = await axios.post(`${baseUrl}`,user);
    return response.data;
};
export const getUserById = async(id) =>{
    let response = await axios.get(`${baseUrl}/${id}`);
    return response.data;
};
export const getUsers = async(request) =>{
    let response = await axios.get(`${baseUrl}${request}`);
    return response.data;
};

export const updateUser = async(data,userId) =>{
    let response = await axios.put(`${baseUrl}/${userId}`,data);
    return response.data;
};

export const promoteUser = async(data,userId) =>{
    let response = await axios.put(`${baseUrl}/promote/${userId}`,data);
    return response.data;
};