import {createRequest,getPaginateData} from '../utils/webApi/requetHelper';
import { ref} from '@vue/composition-api';
export default function usePagination(dataList, endPoint){
    let additionalData = ref({});
    let currentPage = ref(1);
    let totalPageNumber = ref();
    let isLoading = ref(true);
    let searchData =ref();
    const paginateData = (pageNum,searchValue = null)=>{
    searchData.value = searchValue;
    let request = createRequest(searchValue,null,pageNum)
    getPaginateData(endPoint,request).then(response => {
        isLoading.value = false;
        if(response.isSuccess == true){
            dataList.value = response.data;
            additionalData.value = response;
            totalPageNumber.value = Math.ceil(response.totalCount/response.pageSize);
        }else{
            dataList = [];
        }
    });
    }
    const onChangePage = (newPage)=>{
        if(newPage > totalPageNumber.value || newPage < 1)
            return;
        currentPage.value = newPage;
        paginateData(currentPage.value,searchData.value);
    }
    

    return{
        paginateData,
        onChangePage,
        currentPage,
        totalPageNumber,
        additionalData,
        isLoading
    }
}