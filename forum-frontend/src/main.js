import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import interceptorsSetup from '../src/utils/interceptors'
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import Forum from './components/Forum'
import Home from './components/Home'
import Login from './components/Login'
import Register from './components/Register'
import Post from './components/Post'
import CreatePost from './components/Forms/CreatePost'
import Index from './components/Index'
import User from './components/User'
import Users from './components/Users'
import Searched from './components/Searched';
import * as moment from 'moment';
import VueCompositionApi from '@vue/composition-api';
import {getCurrentUser,isAuthenticated} from './utils/webApi/auth';
import {Role} from './utils/Roles';
import CreateForum from './components/Forms/CreateForum';
import NotificationHub from './utils/notification-hub';
import Notifications from 'vue-notification'
Vue.use(NotificationHub);   
Vue.use(VueCompositionApi);
Vue.use(Notifications)
interceptorsSetup();
Vue.use(VueRouter);

Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY HH:mm')
  }
}
);
const routes = [
  { path: '/forum/:id', component: Forum },
  { path: '/forums', component: Home },
  { path: '/', component: Index },
  { path: '/login', component: Login },
  { path: '/register', component: Register },
  { path: '/post/:id', component: Post },
  { path: '/new/post/:forumId', component: CreatePost ,meta:{authorize:[Role.Admin,Role.User]}},
  { path: '/user/:id', component: User },
  { path: '/users', component: Users,meta:{authorize:[Role.Admin]} },
  { path: '/search/:query', component:Searched},
  { path: '/createForum', component: CreateForum,meta:{authorize:[Role.Admin]} },
]

const router = new VueRouter({
  routes,
  mode:'history'
})

router.beforeEach((to,from,next) =>{
  const isLogged = isAuthenticated();
  const loggedUser = getCurrentUser();
  const {authorize} = to.meta;
  if(authorize){
    if(!isLogged){
      return next({path:'/'});
    }

    if(authorize.length && !authorize.includes(loggedUser.role.name)){
      return next({path:'/'});
    }
  }
  next();
});
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
